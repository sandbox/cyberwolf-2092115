<?php
/**
 * @file
 * paddle_promo_box.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function paddle_promo_box_user_default_permissions() {
  $permissions = array();

  // Exported permission: create promo_box content.
  $permissions['create promo_box content'] = array(
    'name' => 'create promo_box content',
    'roles' => array(
      'Chief Editor' => 'Chief Editor',
      'Editor' => 'Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own promo_box content.
  $permissions['delete own promo_box content'] = array(
    'name' => 'delete own promo_box content',
    'roles' => array(
      'Chief Editor' => 'Chief Editor',
      'Editor' => 'Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own promo_box content.
  $permissions['edit own promo_box content'] = array(
    'name' => 'edit own promo_box content',
    'roles' => array(
      'Chief Editor' => 'Chief Editor',
      'Editor' => 'Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit promo_box content in landing pages.
  $permissions['edit promo_box content in landing pages'] = array(
    'name' => 'edit promo_box content in landing pages',
    'roles' => array(
      'Chief Editor' => 'Chief Editor',
      'Editor' => 'Editor',
    ),
    'module' => 'paddle_landing_page',
  );

  // Exported permission: manage paddle_promo_box.
  $permissions['manage paddle_promo_box'] = array(
    'name' => 'manage paddle_promo_box',
    'roles' => array(
      'Chief Editor' => 'Chief Editor',
      'Editor' => 'Editor',
    ),
    'module' => 'paddle_promo_box',
  );

  return $permissions;
}
