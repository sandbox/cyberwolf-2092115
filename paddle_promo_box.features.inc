<?php
/**
 * @file
 * paddle_promo_box.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function paddle_promo_box_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_node_info().
 */
function paddle_promo_box_node_info() {
  $items = array(
    'promo_box' => array(
      'name' => t('Promo box'),
      'base' => 'node_content',
      'description' => t('Use this content type to add a promo-box.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
