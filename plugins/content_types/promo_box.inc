<?php

/**
 * @file
 * Plugin to provide a custom content type pane,
 * for promo box / promo box collection.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Add promo-box'),
  'no title override' => TRUE,
  'description' => t('Add promo-box / promo-box collection.'),
  'category' => t('Paddle'),
);

/**
 * Render callback.
 */
function paddle_promo_box_promo_box_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $block->title = '';

  // Render the promo box.
  $output = '';
  if (!empty($conf['node'])) {
    if (preg_match('/node\/(\d+)/', $conf['node'], $matches)) {
      $nid = $matches[1];
      $node = node_load($nid);
      if ($node) {
        $output .= theme('paddle_promo_box_view', array('node' => $node));
      }
    }
  }

  $block->content = $output;

  return $block;
}

/**
 * Settings form builder callback.
 */
function paddle_promo_box_promo_box_content_type_edit_form($form, &$form_state) {
  $form['promo_box']['#tree'] = TRUE;

  $form['promo_box']['node'] = array(
    '#type' => 'textfield',
    '#title' => t('Select a promo box'),
    '#autocomplete_path' => 'admin/content_manager/add/add-promo-box/autocomplete-callback',
    '#default_value' => isset($form_state['conf']['node']) ? $form_state['conf']['node'] : '',
  );

  return $form;
}

/**
 * Submit callback for the configuration form.
 */
function paddle_promo_box_promo_box_content_type_edit_form_submit($form, &$form_state) {
  foreach (element_children($form['promo_box']) as $key) {
    $form_state['conf'][$key] = $form_state['values']['promo_box'][$key];
  }
}

/**
 * Displays the administrative title for a panel pane in the drag & drop UI.
 */
function paddle_promo_box_promo_box_content_type_admin_title($subtype, $conf, $context) {
  return t('Promo box: %value', array('%value' => $conf['node']));
}
